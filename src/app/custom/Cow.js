import gsap from "gsap/all";
import EventEmitter from "eventemitter3";

export default class Cow extends EventEmitter {
  constructor() {
    super();
    this.cowElement = document.querySelector(".cow");
  }
  static get events() {
    return {
      COW_ABDUCTED: "cow_abducted",
      ABDUCT_COMPLETED: "abduct_completed",
    };
  }
  async moveTo() {
    const tCowAbduction = await gsap.to(this.cowElement, {id:"cowAbduction", y: -390 });
    this.emit(Cow.events.COW_ABDUCTED);
  }
  async toggleCow() {
    const tCowHide = await gsap.to(this.cowElement, { id: "cowHide", opacity: 0 });
    
    this.emit(Cow.events.ABDUCT_COMPLETED);
  }
}
