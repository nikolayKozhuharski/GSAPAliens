import gsap from "gsap/all";
import EventEmitter from "eventemitter3";
import Cow from "./Cow";
export default class Saucer extends EventEmitter {
  constructor() {
    super();
    this._saucerElement = document.querySelector(".ufo");
    this._beamTopElement = document.querySelector("#beam-top");
    this._beamBottomElement = document.querySelector("#beam-bottom");
  }
  static get events() {
    return {
      FLY_IN: "fly_in",
      FLY_AWAY: "fly_away",
      BEAM_SHOW: "beam_showed",
      BEAM_HIDE: "beam_hide",
    };
  }
  async moveTo(x, id) {
    await gsap.to(this._saucerElement, {
      id: id,
      x: x,
    });
  }
  async toggleBeam(opacity, idtop, idBottom) {
    await gsap.to(this._beamTopElement, { id: idtop, opacity: opacity });
    await gsap.to(this._beamBottomElement, {
      id: idBottom,
      opacity: opacity,
    });
  }
}
