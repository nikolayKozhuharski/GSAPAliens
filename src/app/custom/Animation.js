import gsap from "gsap/all";
import EventEmitter from "eventemitter3";
import Saucer from "./Saucer";
import Cow from "./Cow";

export default class Animation extends EventEmitter {
  constructor() {
    super();
    this.saucer = new Saucer();
    this.cow = new Cow();
  }
  async start() {
    await this.saucer.toggleBeam(0, "showTopBeam", "showBottomBeam");
    await this.saucer.moveTo(-835, "flyIn");
    this.saucer.emit(Saucer.events.FLY_IN);
    await this.saucer.toggleBeam(0.6, "showTopBeam", "showBottomBeam");
    this.saucer.emit(Saucer.events.BEAM_SHOW);
    await this.cow.moveTo();
    await this.cow.toggleCow();
    await this.saucer.toggleBeam(0, "hideTopBeam", "hideBottomBeam");
    this.saucer.emit(Saucer.events.BEAM_HIDE);
    await this.saucer.moveTo(-1800, "flyOut");
    this.saucer.emit(Saucer.events.FLY_AWAY);
  }
}
